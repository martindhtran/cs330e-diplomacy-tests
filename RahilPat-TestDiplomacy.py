from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve


class TestDiplomacy(TestCase):
    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n"
        )

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move London\nC London Move Barcelona\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB London\nC Barcelona\n"
        )

    def test_solve_3(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(), "A Barcelona\nB Madrid\n"
        )

    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n"
        )


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


cat TestDiplomacy.tmp
....
----------------------------------------------------------------------
Ran 4 tests in 0.001s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py         107      4     86     12    92%   33->43, 52->23, 55, 56->23, 64->67, 76->exit, 121->124, 128->124, 133, 134->124, 148, 188
TestDiplomacy.py      26      0      0      0   100%
--------------------------------------------------------------
TOTAL                133      4     86     12    93%

"""

