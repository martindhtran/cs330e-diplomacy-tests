#!/usr/bin/env python3



# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold\n"
        a = diplomacy_read(s)
        self.assertEqual(a[0], ["A","Madrid","Hold"])

    def test_read_2(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London"
        a = diplomacy_read(s)
        self.assertEqual(a[0], ["A","Madrid","Hold"])
        self.assertEqual(a[1], ["B","Barcelona","Move","Madrid"])
        self.assertEqual(a[2], ["C","London","Support","B"])
        self.assertEqual(a[3], ["D","Austin","Move","London"])
        try:
            self.assertNotEqual(a[4], [""])
        except:
            pass

    def test_read_3(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n"
        a = diplomacy_read(s)
        self.assertEqual(a[0], ["A","Madrid","Hold"])
        self.assertEqual(a[1], ["B","Barcelona","Move","Madrid"])
        self.assertEqual(a[2], ["C","London","Move","Madrid"])
        try:
            self.assertNotEqual(a[3], [""])
        except:
            pass

    def test_read_4(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n"
        a = diplomacy_read(s)
        self.assertEqual(a[0], ["A","Madrid","Hold"])
        self.assertEqual(a[1], ["B","Barcelona","Move","Madrid"])
        self.assertEqual(a[2], ["C","London","Move","Madrid"])
        self.assertEqual(a[3], ["D","Paris","Support","B"])
        self.assertEqual(a[4], ["E","Austin","Support","A"])
        try:
            self.assertNotEqual(a[5], [""])
        except:
            pass
    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval([
                           ["A","Madrid","Hold"]
                           ])
        self.assertEqual(v,[
                           ["A","Madrid"]
                           ])

    def test_eval_2(self):
        v = diplomacy_eval([
                           ["A","Madrid","Hold"],                \
                           ["B","Barcelona","Move","Madrid"],    \
                           ["C","London","Support","B"],         \
                           ["D","Austin","Move","London"]
                           ])
        self.assertEqual(v,[
                           ["A","[dead]"],      \
                           ["B","[dead]"],      \
                           ["C","[dead]"],      \
                           ["D","[dead]"]
                           ])

    def test_eval_3(self):
        v = diplomacy_eval([
                           ["A","Madrid","Hold"],                  \
                           ["B","Barcelona","Move","Madrid"],      \
                           ["C","London","Move","Madrid"]
                           ])
        self.assertEqual(v,[
                           ["A","[dead]"],      \
                           ["B","[dead]"],      \
                           ["C","[dead]"]
                           ])

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, [["A","Madrid"]])
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, [["A","Madrid"],      \
                            ["B","Barcelona"],   \
                            ["C","London"],      \
                            ["D","Austin"]
                            ])
        self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\nC London\nD Austin\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, [["A","[dead]"],      \
                            ["B","[dead]"],   \
                            ["C","[dead]"]
                            ])
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")


    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

# ----
# main
# ----


if __name__ == "__main__":  # pragma: no cover
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
