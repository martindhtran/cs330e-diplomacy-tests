#!/usr/bin/env python3

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy(TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = ("A Madrid Move London\n"
             "B Barcelona Move Madrid\n"
             "C London Move Barcelona")

        reader = StringIO(s)
        actions = diplomacy_read(reader)
        self.assertEqual(actions[0], "A Madrid Move London")
        self.assertEqual(actions[1], "B Barcelona Move Madrid")
        self.assertEqual(actions[2], "C London Move Barcelona")

    def test_read_2(self):
        s = ("A Hamburg Move Berlin\n"
             "B Berlin Support C\n"
             "C Luxembourg Move Paris")

        reader = StringIO(s)
        actions = diplomacy_read(reader)
        self.assertEqual(actions[0], "A Hamburg Move Berlin")
        self.assertEqual(actions[1], "B Berlin Support C")
        self.assertEqual(actions[2], "C Luxembourg Move Paris")

    def test_read_3(self):
        s = ("A Calais Move Paris\n"
             "B Paris Hold")

        reader = StringIO(s)
        actions = diplomacy_read(reader)
        self.assertEqual(actions[0], "A Calais Move Paris")
        self.assertEqual(actions[1], "B Paris Hold")

    # ----
    # eval
    # ----

    def test_eval_1(self):
        actions = [
            "A Madrid Move London",
            "B Barcelona Move Madrid",
            "C London Move Barcelona"
        ]

        outcome = [
            "A London",
            "B Madrid",
            "C Barcelona"
        ]

        result = diplomacy_eval(actions)
        self.assertEqual(result, outcome)

    def test_eval_2(self):
        actions = [
            "A Madrid Hold",
            "B Barcelona Move Madrid",
            "C London Move Madrid"
        ]
        outcome = [
            "A [dead]",
            "B [dead]",
            "C [dead]"
        ]

        result = diplomacy_eval(actions)
        self.assertEqual(result, outcome)

    def test_eval_3(self):
        actions = [
            "A Paris Hold",
            "B London Move Paris",
            "C Madrid Move Paris",
            "D Barcelona Support A"
        ]
        outcome = [
            "A Paris",
            "B [dead]",
            "C [dead]",
            "D Barcelona"
        ]

        result = diplomacy_eval(actions)
        self.assertEqual(result, outcome)

    def test_eval_4(self):
        actions = [
            "A Barcelona Hold",
            "B Madrid Move Barcelona",
            "C Paris Move Barcelona",
            "D London Support B",
            "E Prague Support C"
        ]
        outcome = [
            "A [dead]",
            "B [dead]",
            "C [dead]",
            "D London",
            "E Prague"
        ]

        result = diplomacy_eval(actions)
        self.assertEqual(result, outcome)

    def test_eval_5(self):
        actions = [
            "A Dallas Support D",
            "B Austin Support C",
            "C Houston Hold",
            "D Paris Move Houston",
            "E Ennis Move Dallas"
        ]
        outcome = [
            "A [dead]",
            "B Austin",
            "C Houston",
            "D [dead]",
            "E [dead]"
        ]

        result = diplomacy_eval(actions)
        self.assertEqual(result, outcome)

    def test_eval_6(self):
        actions = [
            "A Dallas Move Austin",
            "B Austin Support A"
        ]
        outcome = [
            "A [dead]",
            "B [dead]"
        ]

        result = diplomacy_eval(actions)
        self.assertEqual(result, outcome)
    # -----
    # print
    # -----

    def test_print_1(self):
        outcome = [
            "A [dead]",
            "B Austin",
            "C Houston",
            "D [dead]",
            "E [dead]"
        ]

        output = (
            "A [dead]\n"
            "B Austin\n"
            "C Houston\n"
            "D [dead]\n"
            "E [dead]\n"
        )

        w = StringIO()
        diplomacy_print(w, outcome)

        self.assertEqual(w.getvalue(), output)

    def test_print_2(self):
        outcome = [
            "A Paris"
        ]

        output = (
            "A Paris\n"
        )

        w = StringIO()
        diplomacy_print(w, outcome)

        self.assertEqual(w.getvalue(), output)

    def test_print_3(self):
        outcome = [
            "A Barcelona",
            "B [dead]",
            "C Beijing",
            "D Guandong"
        ]

        output = (
            "A Barcelona\n"
            "B [dead]\n"
            "C Beijing\n"
            "D Guandong\n"
        )

        w = StringIO()
        diplomacy_print(w, outcome)

        self.assertEqual(w.getvalue(), output)

    # -----
    # solve
    # -----

    def test_solve_1(self):
        s_input = ("A Paris Hold\n"
                   "B London Move Paris\n"
                   "C Madrid Move Paris\n"
                   "D Barcelona Support A")

        s_output = ("A Paris\n"
                    "B [dead]\n"
                    "C [dead]\n"
                    "D Barcelona\n")

        r = StringIO(s_input)
        w = StringIO()

        diplomacy_solve(r, w)

        self.assertEqual(w.getvalue(), s_output)

    def test_solve_2(self):
        s_input = ("A Austin Move Dallas\n"
                   "B Dallas Move Paris\n"
                   "C Paris Move Ennis\n"
                   "D Ennis Move Austin")

        s_output = ("A Dallas\n"
                    "B Paris\n"
                    "C Ennis\n"
                    "D Austin\n")

        r = StringIO(s_input)
        w = StringIO()

        diplomacy_solve(r, w)

        self.assertEqual(w.getvalue(), s_output)

    def test_solve_3(self):
        s_input = ("A Vienna Support B\n"
                   "B Budapest Move Vienna\n"
                   "C Sydney Support B\n"
                   "D Prague Support A")

        s_output = ("A [dead]\n"
                    "B [dead]\n"
                    "C Sydney\n"
                    "D Prague\n")

        r = StringIO(s_input)
        w = StringIO()

        diplomacy_solve(r, w)

        self.assertEqual(w.getvalue(), s_output)

# ----
# main
# ----


if __name__ == "__main__": # pragma: no cover
    main()
