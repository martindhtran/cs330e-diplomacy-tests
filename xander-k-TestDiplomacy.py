#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve


# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # diplomacy_solve
    # ----
    def test_diplomacy_solve_1(self):  # one army
        r = StringIO("A Madrid Hold")
        w = diplomacy_solve(r)
        self.assertEqual(w, "A Madrid")
        
    def test_diplomacy_solve_2(self):  # one army
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        w = diplomacy_solve(r)
        self.assertEqual(w, "A [dead]\nB Madrid\nC London")
        
    def test_given_3(self):  # given
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = diplomacy_solve(r)
        self.assertEqual(w, "A [dead]\nB [dead]")
        
    # FIX ME!!!
    # def test_given_4(self):  # given
    #     r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London")
    #     w = diplomacy_solve(r)
    #     self.assertEqual(w,"A [dead]\nB [dead]\nC [dead]\nD [dead]")
        
    def test_given_5(self):  # given
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid")
        w = diplomacy_solve(r)
        self.assertEqual(w, "A [dead]\nB [dead]\nC [dead]")

    def test_given_6(self):  # given
        r = StringIO( "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B")
        w = diplomacy_solve(r)
        self.assertEqual(w, "A [dead]\nB Madrid\nC [dead]\nD Paris")

    def test_given_7(self):  # given
        r = StringIO( "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = diplomacy_solve(r)
        self.assertEqual(w, "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin")

    def test_diplomacy_1(self):
        r = StringIO("A Austin Hold\nB Boston Hold\nC ColoradoSprings Hold\nD Dallas Hold\nE Eugene Hold\nF FortCollins Hold\nG GeorgeTown Hold\nH Houston Hold")
        w = diplomacy_solve(r)
        self.assertEqual(w, "A Austin\nB Boston\nC ColoradoSprings\nD Dallas\nE Eugene\nF FortCollins\nG GeorgeTown\nH Houston")

    def test_diplomacy_2(self):  # asserting that the input is not empty
        r = StringIO("")
        w = diplomacy_solve(r)
        self.assertEqual(w, "")

    def test_diplomacy_3(self):
        r = StringIO("A Austin Hold\nB Boston Move Austin\nC ColoradoSprings Move Austin\nD Dallas Move Austin\nE Eugene Move Austin\nF FortCollins Move Austin\nG GeorgeTown Move Austin\nH Houston Move Austin")
        w = diplomacy_solve(r)
        self.assertEqual(w, "A [dead]\nB [dead]\nC [dead]\nD [dead]\nE [dead]\nF [dead]\nG [dead]\nH [dead]")

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1
$ coverage report -m

$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
